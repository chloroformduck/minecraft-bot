import logging
import os
from db import Botdb
from config import ConfigReader
from mctools import RCONClient, errors
from interactions import Extension, OptionType, SlashContext, SlashCommandChoice, slash_command, slash_option
from interactions.client.utils import get

logging.getLogger("Minecraft")

config = ConfigReader()

guild: int = config.get('guild')
minecraft_servers: list[dict] = config.get('minecraft_servers')
database = Botdb(config.get('db_path'), guild)

class Minecraft(Extension):
    def __init__(self, bot) -> None:
        self.bot = bot

    def _connect(self, host, pword):
        client = RCONClient(host)

        if client.login(pword):
            return client
        else:
            raise errors.RCONAuthenticationError
        
    def _sendCommand(self, command: str, host: str, pword: str) -> str:
        client = self._connect(host, pword)
        response = client.command(command)
        client.stop()
        return response[:-4] # response is formatted like it's from a mainframe from the 80's so we just want the important bit, the actual status from the command
    
    # I do this because the other option is to pass discord the full dictionary, but that would expose the rcon pass to discord
    def _get_server(self, server: str) -> dict:
        "Takes a server name and returns the dictionary of server name and password"
        for mcserver in minecraft_servers:
            if mcserver['name'] == server:
                return mcserver

    @slash_command(name="whitelist", scopes = [guild], description="Whitelist yourself on one of the Minecraft servers")
    @slash_option(
        name="username",
        description="Your Mojang or Microsoft account name that you use to log into the launcher",
        opt_type=OptionType.STRING,
        required=True
    )
    @slash_option(
        name="server",
        description="The server you want to be whitelisted on",
        required=True,
        opt_type=OptionType.STRING,
        choices=[
            SlashCommandChoice(
                name=server['name'],
                value=server['name']
            ) for server in minecraft_servers
        ]
    )
    async def whitelist(self, ctx: SlashContext, username: str, server: str):
        "Takes a username and whitelists it on the selected server. Also sends a log message to log channel"
        author = ctx.author
        lc = database.get_guild_channel("log")
        if lc:
            log_channel = get(ctx.guild.channels, object_id=lc)
            await log_channel.send(f"{author} has requested whitelisting on {server}")
        command = f"whitelist add {username}"
        response = self._sendCommand(command, server, self._get_server(server)['password'])
        await ctx.send(response, ephemeral=True)

def setup(bot):
    Minecraft(bot)