import logging
import yaml
from pathlib import Path

logging.getLogger('DB')

class Botdb():
    def __init__(self, db_file: str, guild: int) -> None:
        self.db_file = db_file
        if not Path(db_file).is_file():
            logging.info("DB file not found. Initializing...")
            with open(Path(db_file), 'w') as f:
                yaml.safe_dump({"guild": guild, "options":[], "channels": {}}, f)
        with open(Path(db_file), 'r') as f:
            self.data_stream: dict = yaml.safe_load(f)

    def _save(self) -> None:
        "Saves the currently loaded data stream back to the file"
        with open(self.db_file, 'w') as f:
            yaml.safe_dump(self.data_stream, f)

    def _set_channel(self, action: str, channel_id = None|int) -> None:
        "Sets a channel to the given action"
        self.data_stream['channels'][action] = channel_id
        self._save()

    def check_guild_option(self, option: str) -> bool:
        "Checks if an option is enabled for your guild"
        return True if option in self.data_stream['options'] else False

    def toggle_guild_option(self, option: str) -> None:
        "Toggles an option for your guild"
        if self.check_guild_option(option):
            self.data_stream['options'].remove(option)
        else:
            self.data_stream['options'].append(option)
        self._save()

    def get_guild_channel(self, action: str) -> int|None:
        "Takes a guild and an action and returns the channel_id, if it exists"
        return self.data_stream['channels'].get(action)

    def set_admin_channel(self, channel_id: int) -> None:
        "Sets the admin channel for your guild"
        self._set_channel('admin', channel_id)

    def set_log_channel(self, channel_id: int) -> None:
        "Sets the log channel for your guild"
        self._set_channel('log', channel_id)