import logging
import os
from interactions import Client, Intents
from config import ConfigReader

extensions = ['listeners', 'minecraft', 'admin']

conf = ConfigReader('config.yml')

environ = conf.get('environment')

if environ == 'prod':
    loglevel = "warning"
else:
    loglevel = "info"
numeric_level = getattr(logging, loglevel.upper(), None)
logging.basicConfig(format='%(asctime)s|%(levelname)s|%(name)s:  %(message)s', filename='bot.log', level=numeric_level, datefmt='%Y-%m-%d %H:%M:%S')


bot = Client(intents=Intents.DEFAULT)

guild = conf.get('guild')
token = conf.get('token')

for extension in extensions:
    bot.load_extension(extension)
bot.start(token)
