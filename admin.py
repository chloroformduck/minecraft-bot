import logging
from db import Botdb
from config import ConfigReader
from interactions import Permissions, GuildText, Extension, SlashContext, OptionType, ChannelType, slash_command, slash_option

logging.getLogger("Admin")

config = ConfigReader()

guild: int = config.get('guild')
database = Botdb(config.get('db_path'), guild)

class Admin(Extension):
    def __init__(self, bot) -> None:
        self.bot = bot

    @slash_command(name="set_log_channel", scopes = [guild], description="Set the log channel. It is recommended to use a channel only visible to moderators", default_member_permissions=Permissions.ADMINISTRATOR)
    @slash_option(name="channel", description="The channel to set as the log chanel", opt_type=OptionType.CHANNEL, required=True, channel_types=[ChannelType.GUILD_TEXT])
    async def set_log_channel(self, ctx: SlashContext, channel: GuildText):
        "Takes a channel and sets it as the log channel"
        try:
            database.set_log_channel(int(channel.id))
            await ctx.send(f"Set the log channel to {channel}", ephemeral=True)
        except Exception as e:
            logging.error(f"Tried setting the log channel and got: {e}")
            await ctx.send("Failed to set the log channel", ephemeral=True)

def setup(bot):
    Admin(bot)