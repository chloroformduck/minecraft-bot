import logging
from datetime import datetime
from config import ConfigReader
from db import Botdb
from interactions import Extension, listen

logging.getLogger("Listeners")

config = ConfigReader()

guild: int = config.get('guild')
minecraft_servers: list = config.get('minecraft_servers')
database = Botdb(config.get('db_path'), guild)

class Listeners(Extension):
    def __init__(self, bot) -> None:
        self.bot = bot

    @listen()
    async def on_ready(self):
        logging.info(f"Bot started at {datetime.now()} for {guild}")

def setup(bot):
    Listeners(bot)